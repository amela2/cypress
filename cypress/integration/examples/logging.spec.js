/// <reference types="Cypress" />

describe ('Applitools app for testing', function() {

    beforeEach (function () {
        cy.visit('https://demo.applitools.com/')
      });

    it('1.Verify logging with visible credentials', function() {
        cy.get('#username').type('trla')
        cy.get('#password').type('mrla')
        cy.contains('#log-in', 'Sign in').click()
        cy.get('.logged-user-w').should('be.visible')   
    });  

    it('2.Verify logging using env variables', function() {
 
        var username = Cypress.env('USERNAME')
        var password = Cypress.env('PASSWORD')
 
        cy.login(username, password)
        cy.contains('#log-in', 'Sign in').click()
        cy.get('.logged-user-w').should('be.visible')
     });  

});
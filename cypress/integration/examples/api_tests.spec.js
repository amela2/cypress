/// <reference types="Cypress" />

describe ('API tests - GET method', function() {

    beforeEach (function () {
      cy.request('/').as('Main page')
    });

    it('1.Verify that correct status code is returned', function() {
       cy.get('@Main page').its('status').should('be.equal', 200) 
    });  

    it('2.Verify that headers content type is text/html data', function() {
      cy.get('@Main page').its('headers').its('content-type').should('include', 'text/html') 
   }); 
   
}); 
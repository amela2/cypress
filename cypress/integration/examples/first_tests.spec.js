/// <reference types="Cypress" />

describe ('Regent app', function() {

    beforeEach (function () {
      cy.visit('/')
 //    cy.visit(Cypress.env('PRODUCTION_ENV'))
    });

    it('1.Verify the page contains the correct text', function() {
       cy.get('h1').eq(1).invoke('text').as('Header2')
  //     cy.screenshot('/holePage') 
  //     cy.get('h1').eq(1).invoke('text')
       cy.get('@Header2')   
       cy.should('equal','IT som det borde vara');
    });        

    it('2.Verify that navigation to Test offer works', ()=>{
        cy.get('.dropdown-menu').as('Dropdown')
        
        cy.clickLink('Erbjudande').trigger('mouseover')
          .debug()
          .get('@Dropdown').contains('Test och automatisering').click({force: true})
          .get('h2').eq(0).invoke('text')
          .should('equal','Automatiserade leveransflöden med kvalitet')
          .location('pathname').should('equal', '/erbjudande/test-och-automatisering/')
     });      

     it('3.Verify that Job page contains link for job search', ()=>{
      cy.clickLink('Jobb')
      cy.location('pathname').should('equal', '/jobb/');
      cy.contains('[href="http://sverigesbastamedarbetare.se"]', 'Besök vår jobbsajt').should('have.attr', 'href')
   });  

     it('4.Verify that customer can register for assignment requests', ()=>{
      cy.clickLink('Uppdrag')
      cy.get('#Name').as('Name')
      cy.get('#Email').as('Email')

      var name = 'Test'
      var randomNo = Math.floor(10000 * Math.random() + 1).toString();
      var email = 'test' + randomNo + '@test.com'

      cy.get('@Name').type(name)
        .get('@Email').type(email)
        .get('#registerNewsletter').click()
      cy.contains('.toast-message', 'Tack för visat intresse.').should('be.visible')
 }); 
      
 it('5.Verify that Arbetsmiljöpolicy document can be extended and collapsed', ()=>{
      cy.clickLink('Om oss')
        .get('.read-link').eq(2).click({force: true})
        .clickLink('Läs mindre')
        .contains('.readAllLink2 ', 'Läs mer').should('be.visible')  
 }); 

});

